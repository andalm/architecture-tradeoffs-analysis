# Architecture Tradeoffs Analysis

This is a live challenge to check your architectural analysis experience, please:

1. Read the proposed Neobank architecture description
2. Fill the [analysis.md](analyysis.md) file with your analysis over the proposal
3. Copy the original [Plantuml](https://plantuml.com/component-diagram) component [diagram](architecture.puml) and modify it with your proposed improvements (use actual AWS tooling, [Sample](https://crashedmind.github.io/PlantUMLHitchhikersGuide/aws/aws.html#create-real-life-aws-diagrams), [AWS Icons](https://github.com/awslabs/aws-icons-for-plantuml/blob/master/AWSSymbols.md)) 
4. Make a pull request to this repo (branch develop) with your answers
5. Please, don't use chatGPT...

## Neobank Architecture

By [Mikhail Sadovskiy](https://www.linkedin.com/in/mikhailsadovskiy/)

Senior Software Engineer, Technical Architect, Java, Cloud, AWS, FinTech

Implementation of a safe and legal banking platform is a challenging task from regulatory compliance, technical and operational perspectives. There are several approaches to building a modern digital banking platform:
1. Customizing/white-labeling existing commercial BAAS platform, for example, Mambu banking. 
2. Building on top of an open-source banking platform such as Apache Fineract/Mifos.
3. Developing from scratch using fintech APIs.

This article discusses the third option of building a banking platform from scratch using a public cloud provider and third-party APIs to simplify the process and offload complicated functionality to third-party vendors.  Below are architecture principles for building a scalable, easy-to-support, and extend with the new functionality system.
1. The system is built by orchestrating third-party API calls such as payment processors, KYC providers, card issuing services, etc.
2. It should be designed in a way that accounts for possible downtimes, connectivity issues and failures when calling external APIs
3. The transfer functionality is implemented by using an external payment processor via an ACH payment. Clients’ accounts are held in one or multiple partner banks.
4. Microservices architecture is used for independent deployments and scalability of separate components based on the load.

The system is composed of modules that can be added to the core to extend it with additional functionality.

An external payment processor that supports ACH is used to handle domestic transfers. It could be one of the traditional platforms used by banks for high-volume payments such as Fiserv, JHA, and TSYS. It could also be one of the modern API providers such Dwolla or Sila. Most US-based neobanks partner with traditional banks such as Chase or Wells Fargo to hold their accounts. For example, the largest digital bank by number of account holders in the US Chime partners with Bancorp and Stride banks. Ideally, the system should be designed to allow for switching between different payment processors and partner banks or using several of them at the same time. 

The backend consists of microservices responsible for specific functionality such as account management, transfers, deposits, etc. The core components responsible for account balances and transactions are often referred to as the ledger service.
The design of transfers includes two main flows:
1. outbound flow when a transfer is initiated by a user 
2. inbound flow when a transfer is initiated outside the system
When the system is notified about an external event such as transfers and card transactions, the event is queued first and then processed asynchronously by the inbound processor.

![Architecture Proposal](architecture.svg)